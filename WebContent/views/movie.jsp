<html>

<head>
	<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
	<link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" type="text/css" rel="stylesheet">
</head>

<body>

<h1>${ movie.title }</h1>

<img src="${ movie.bannerUrl }">
<br>
<a href="${ movie.bannerUrl }">${ movie.bannerUrl }</a>

<table class="table">
	<tr>
		<th>Id</th>
		<td>${ movie.id }</td>
	</tr>
	<tr>
		<th>Year</th>
		<td>${ movie.year }</td>
	</tr>
	<tr>
		<th>Director</th>
		<td>${ movie.director }</td>
	</tr>
	<tr>
		<th>Genres</th>
		<td>
			<c:forEach items="${ movie.genres }" var="genre">
				<a href="/CS122B-02/search?genre=${ genre.id }">${ genre.name }</a><span>,</span>
			</c:forEach>
		</td> 
	</tr>
	<tr>
		<th>Stars</th>
		<td>
			<c:forEach items="${ movie.stars }" var ="star">
				<a href="/CS122B-02/stars?star=${ star.id }">${ star.name }</a><span>,</span>
			</c:forEach>
		</td>
	</tr>
	<tr>
		<th>Trailer</th>
		<td>
			<a href="${ movie.trailerUrl }">${ movie.trailerUrl }</a>
		</td>
	</tr>
</table>	


</body>
</html>