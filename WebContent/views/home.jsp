<html>
<head>
	<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<link href="/CS122B-02/css/home.css" type="text/css" rel="stylesheet">
	<link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" type="text/css" rel="stylesheet">
</head>
<body>

<div class="jumbotron">
	<h1>MovieDB</h1>
	<p>The simple movie database</p>
</div>
<div id="search-form" style="text-align:center;">
	<form name="search" action="/CS122B-02/search" method="get">
	  <input type="text" name="search" placeholder="Search by movie, year, director, or actor" style="font-size:30px;height:50px; width:600px;">
	  <input type="submit" value="Search">
	</form>
</div>

<div id="browse-genre" style="float:left;margin-left:15px;display:inline-block">
	<span class="lead">Browse by Genre</span>
	<ul style="padding:30px;list-style:none;height:500px;width:500px;font-size:24px;border:1px solid #eee;overflow:scroll">
		<c:forEach items="${ genres }" var="genre">
			<li><a href="/CS122B-02/search?genre=${ genre.id }">${ genre.name }</a></li>
		</c:forEach>
	</ul>
</div>

<div id="browse-title" style="float:left;margin-left:15px;display:inline-block">
	<span class="lead">Browse by Title</span>
	<ul style="padding:30px;list-style:none;height:500px;width:500px;font-size:24px;border:1px solid #eee;overflow:scroll">
		<li><a href="/CS122B-02/search?title=A">A</a></li>
		<li><a href="/CS122B-02/search?title=B">B</a></li>
		<li><a href="/CS122B-02/search?title=C">C</a></li>
		<li><a href="/CS122B-02/search?title=D">D</a></li>
		<li><a href="/CS122B-02/search?title=E">E</a></li>
		<li><a href="/CS122B-02/search?title=F">F</a></li>
		<li><a href="/CS122B-02/search?title=G">G</a></li>
		<li><a href="/CS122B-02/search?title=H">H</a></li>
		<li><a href="/CS122B-02/search?title=I">I</a></li>
		<li><a href="/CS122B-02/search?title=J">J</a></li>
		<li><a href="/CS122B-02/search?title=K">K</a></li>
		<li><a href="/CS122B-02/search?title=L">L</a></li>
		<li><a href="/CS122B-02/search?title=M">M</a></li>
		<li><a href="/CS122B-02/search?title=N">N</a></li>
		<li><a href="/CS122B-02/search?title=O">O</a></li>
		<li><a href="/CS122B-02/search?title=P">P</a></li>
		<li><a href="/CS122B-02/search?title=Q">Q</a></li>
		<li><a href="/CS122B-02/search?title=R">R</a></li>
		<li><a href="/CS122B-02/search?title=S">S</a></li>
		<li><a href="/CS122B-02/search?title=T">T</a></li>
		<li><a href="/CS122B-02/search?title=U">U</a></li>
		<li><a href="/CS122B-02/search?title=V">V</a></li>
		<li><a href="/CS122B-02/search?title=X">W</a></li>
		<li><a href="/CS122B-02/search?title=Y">X</a></li>
		<li><a href="/CS122B-02/search?title=Z">Y</a></li>
		<li><a href="/CS122B-02/search?title=0">0</a></li>
		<li><a href="/CS122B-02/search?title=1">1</a></li>
		<li><a href="/CS122B-02/search?title=2">2</a></li>
		<li><a href="/CS122B-02/search?title=3">3</a></li>
		<li><a href="/CS122B-02/search?title=4">4</a></li>
		<li><a href="/CS122B-02/search?title=5">5</a></li>
		<li><a href="/CS122B-02/search?title=6">6</a></li>
		<li><a href="/CS122B-02/search?title=7">7</a></li>
		<li><a href="/CS122B-02/search?title=8">8</a></li>
		<li><a href="/CS122B-02/search?title=9">9</a></li>		
	</ul></div>

</body>
</html>