<html>

<head>
	<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
	<link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" type="text/css" rel="stylesheet">
	<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
</head>

<script>
	
	$(function() {
		var type = ${ sort };
		var num = ${ numResults };
		var p = ${ page };
		console.log(type);
		var sorts = {
			"title-asc": "Title ASC",
			"title-desc": "Title DESC",
			"year-asc": "Year ASC",
			"year-desc": "Year DESC",
		};
		var results = [10, 25, 50, 100];
		
		for(var k in sorts) {
			var sort = sorts[k];
			var btn = "btn-default";
			if(k == type) {
				btn = "btn-primary";
			}
			var $a = $('<a role="button" class="btn ' + btn + '" href="' + sortUrl('sort', k) +'">' + sort + '</a>');
			$('#sorting').append($a);
		}
		
		for(var k in results) {
			var result = results[k];
			var btn = 'btn-default';
			if(result == num) {
				btn = 'btn-primary'
			}
			var url = sortUrl('page', 1);
			var $a = $('<a role="button" class="btn ' + btn + '" href="' + sortUrl('numResults', result, url) +'">' + result + '</a>');
			console.log($a);
			$('#results').append($a);			
		}
		
		$('#prev').attr('href', sortUrl('page',parseInt(p)-1));
		$('#next').attr('href', sortUrl('page',parseInt(p)+1));
		
		function sortUrl(k, s, n) {
			var path = n ? n : document.URL.split('/').slice(-1)[0];
			var params = path.split('?')[1].split('&');
			var url = path.split('?')[0] + '?';
			var replaced = false;
			var prefix = '';
			for(var i in params) {
				var piece = params[i].split('=');
				var key = piece[0];
				var val = piece[1];
				
				if(key == k) {
					val = s;
					replaced = true;
				}
				url += prefix + key + '=' + val;
				prefix = '&';
			}
			if(!replaced) {
				url += '&' + k + '=' + s;
			}
			return url;
		}
	});

</script>

<body>

<h1>Search results</h1>

<c:if test="${ fn:length(movies) == 0 }">
	<em>No results found</em>
</c:if>

<c:if test="${ fn:length(movies) > 0 }">

<div id="sorting" style="display:inline-block; float:left;'">
	<span class="lead">Sort: </span>
</div>

<div id="results" style="display:inline-block; float:right">
	<span class="lead">Results per page:</span>
</div>

<table class="table">
	<tr>
		<th>ID</th>
		<th>Title</th>
		<th>Year</th>
		<th>Director</th>
		<th>Genres</th>
		<th>Stars</th>
	</tr>
	<c:forEach items="${ movies }" var="movie">
		<tr>
			<td>${ movie.id }</td>
			<td>
				<a href="/CS122B-02/movies?movie=${ movie.id }">${ movie.title }</a>
			</td>
			<td>${ movie.year }</td>
			<td>${ movie.director }</td>
			<td>
				<c:forEach items="${ movie.genres }" var="genre">
					${ genre.name } <span>,</span>
				</c:forEach>
			</td> 
			<td>
				<c:forEach items="${ movie.stars }" var ="star">
					<a href="/CS122B-02/stars?star=${ star.id }">${ star.name }</a><span>,</span>
				</c:forEach>
			</td>
		</tr>
	</c:forEach>
</table>

<div id="nav" class="lead" style="text-align:center">
	<a id="prev">&lt;&nbsp;Prev</a>
	<span>&nbsp;&nbsp;|&nbsp;&nbsp;</span>
	<a id="next">Next&nbsp;&gt;</a>
	
</div>
</c:if>


</body>
</html>