<html>

<head>
	<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
	<link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" type="text/css" rel="stylesheet">
</head>

<body>

<h1>${ star.name }</h1>

<img src="${ star.photoUrl }">
<br>
<a href="${ star.photoUrl }">${ star.photoUrl }</a>

<table class="table">
	<tr>
		<th>Id</th>
		<td>${ star.id }</td>
	</tr>
	<tr>
		<th>DOB</th>
		<td>${ star.dob }</td>
	</tr>
	<tr>
		<th>Movies</th>
		<td>
			<c:forEach items="${ star.movies }" var ="movie">
				<a href="/CS122B-02/movies?movie=${ movie.id }">${ movie.title }</a><span>,</span>
			</c:forEach>
		</td>
	</tr>
</table>	


</body>
</html>