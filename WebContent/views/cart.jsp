<html>

<head>
	<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
	<link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" type="text/css" rel="stylesheet">
</head>

<body>

<h1>Cart</h1>

<table class="table">
	<tr>
		<th>Movie Title</th>
		<th>Qty</th>
	</tr>
	<c:forEach items="${ movies }" var="movie">
		<tr>
			<th>${ movie.title }</th>
			<th>${ movie.qty }</th>
		</tr>
	</c:forEach>
</table>

<a href="/CS122B-02/checkout">Checkout</a>


</body>
</html>