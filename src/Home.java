

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Models.Genre;

/**
 * Servlet implementation class Home
 */
@WebServlet("/")
public class Home extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Home() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList<Genre> genres = new ArrayList<Genre>();
		genres = getGenres();
		request.setAttribute("genres", genres);
		
		request.getRequestDispatcher("/views/home.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	private ArrayList<Genre> getGenres() {
		ArrayList<Genre> genres = new ArrayList<Genre>();

		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();	
	        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/moviedb", 
	        		"root", "1ndoviet");
	        
	        Statement stmt = connection.createStatement();
	        ResultSet results = stmt.executeQuery("SELECT * FROM genres");
	        
	        while (results.next()) {
	        	Genre genre = new Genre();
	        	genre.id = results.getInt("id");
	        	genre.name = results.getString("name");
	        	
	        	genres.add(genre);
	        }
	        
	        connection.close();
	        stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return genres;
	}
}
