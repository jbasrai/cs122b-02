import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Models.Genre;
import Models.Movie;
import Models.Star;

/**
 * Servlet implementation class Cart
 */
@WebServlet("/checkout")
public class Checkout extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Checkout() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/views/checkout.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String cc = request.getParameter("cc");
		String exp = request.getParameter("exp");
		String first_name = request.getParameter("first_name");
		String last_name = request.getParameter("last_name");
		
		String message = "";
		
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();	
	        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/moviedb", 
	        		"root", "1ndoviet");
	        
	        PreparedStatement statement = connection.prepareStatement("SELECT * FROM creditcards "
	        		+ "WHERE id = ? AND expiration = ? AND first_name = ? AND last_name = ?");
	        
	        statement.setString(1, cc);
	        statement.setString(2, exp);
	        statement.setString(3, first_name);
	        statement.setString(4, last_name);
	        ResultSet results = statement.executeQuery();
	        
	        if(results.next()) {
	        	// empty cart
	    		response.addCookie(new Cookie("cart", ""));
	    		message = "Checkout successful.";
	        }
	        else {
	        	//not authorized
	        	message = "Checkout unsuccessful!";
	        }
	        connection.close();
		} catch(Exception e) {
			e.printStackTrace();
		}

    	request.setAttribute("message", message);
		request.getRequestDispatcher("/views/checkout.jsp").forward(request, response);
	}

}