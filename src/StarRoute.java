

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Models.Genre;
import Models.Movie;
import Models.Star;

/**
 * Servlet implementation class StarRoute
 */
@WebServlet("/stars")
public class StarRoute extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StarRoute() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("star");
		if(id == null) {
			request.setAttribute("star", null);
			request.getRequestDispatcher("/views/star.jsp").forward(request, response);
			return;
		}
		
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();	
	        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/moviedb", 
	        		"root", "1ndoviet");
	        
	        StringBuffer query = new StringBuffer();
	        query.append("SELECT stars.id, first_name, last_name, dob, photo_url,\n");
	        query.append("GROUP_CONCAT(movies.id SEPARATOR ',') AS mIds,\n");
	        query.append("GROUP_CONCAT(movies.title SEPARATOR ',') AS mTitles\n");
	        query.append("FROM stars\n");
	        query.append("JOIN stars_in_movies ON stars_in_movies.star_id = stars.id\n");
	        query.append("JOIN movies ON stars_in_movies.movie_id = movies.id\n");
	        query.append("WHERE stars.id = ? \n");
	     
	        PreparedStatement statement = connection.prepareStatement(query.toString());
	        statement.setInt(1, Integer.parseInt(id));
	        ResultSet results = statement.executeQuery();
	        results.next();
	        
	        Star star = new Star();
	        star.id = results.getInt("stars.id");
	        star.name = results.getString("first_name") + " " + results.getString("last_name");
	        star.dob = results.getDate("dob");
	        star.photoUrl = results.getString("photo_url");
	        
	        ArrayList<Movie> movies = new ArrayList<Movie>();
	        String[] mIds = results.getString("mIds").split(",");
	        String[] mTitles = results.getString("mTitles").split(",");
	        for(int i=0; i < mIds.length; i++) {
	        	Movie movie = new Movie();
	        	movie.id = Integer.parseInt(mIds[i]);
	        	movie.title = mTitles[i];
	        	movies.add(movie);
	        }
	        star.movies = movies;
	        
	        request.setAttribute("star", star);
	        request.getRequestDispatcher("/views/star.jsp").forward(request, response);
	        
	        connection.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
