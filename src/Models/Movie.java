package Models;
import java.util.ArrayList;


public class Movie {
	
	public int id, year, qty;
	public String title, director, bannerUrl, trailerUrl;
	public ArrayList<Star> stars;
	public ArrayList<Genre> genres;
	
	public Movie() {
		stars = new ArrayList<Star>();
		genres = new ArrayList<Genre>();
	}
	
	public int getId() {
		return id;
	}
	
	public int getYear() {
		return year;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getDirector() {
		return director;
	}
	
	public String getBannerUrl() {
		return bannerUrl;
	}
	
	public String getTrailerUrl() {
		return trailerUrl;
	}
	
	public ArrayList<Star> getStars() {
		return stars;
	}
	
	public ArrayList<Genre> getGenres() {
		return genres;
	}
	
	public int getQty() {
		return qty;
	}
}
