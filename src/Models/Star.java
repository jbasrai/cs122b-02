package Models;

import java.sql.Date;
import java.util.ArrayList;

public class Star {
	
	public int id;
	public String name, photoUrl;
	public Date dob;
	public ArrayList<Movie> movies;
	
	public Star() {
		movies = new ArrayList<Movie>();
	}
	
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	public String getPhotoUrl() {
		return photoUrl;
	}
	
	public Date getDob() {
		return dob;
	}
	
	public ArrayList<Movie> getMovies() {
		return movies;
	}
	
}