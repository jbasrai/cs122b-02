

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Models.Genre;
import Models.Movie;
import Models.Star;

/**
 * Servlet implementation class Search
 */
@WebServlet("/search")
public class Search extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Search() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String sort = request.getParameter("sort") == null ? "'title-asc'" : "'" + request.getParameter("sort") + "'";
		String page = request.getParameter("page") == null ? "1" : "'" + request.getParameter("page") + "'";
		String numResults = request.getParameter("numResults") == null ? "10" : "'" + request.getParameter("numResults") + "'";
		
		ArrayList<Movie> movies = getMovieList(request);
		request.setAttribute("movies", movies);
		request.setAttribute("sort", sort);
		request.setAttribute("page", page);
		request.setAttribute("numResults", numResults);
		request.getRequestDispatcher("/views/search.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	private ArrayList<Movie> getMovieList(HttpServletRequest request) {
        ArrayList<Movie> movies = new ArrayList<Movie>();
		
        String search = request.getParameter("search");
		String genre = request.getParameter("genre");
		String title = request.getParameter("title");
		
		String sort = mapSort(request.getParameter("sort"));
		int numResults = mapNumResults(request.getParameter("numResults"));
		int page = mapPage(request.getParameter("page"));
		
		int offset = numResults * (page - 1);
		
		ResultSet results = null;
		if(search != null) {
			results = search(search, sort, numResults, offset);
		}
		else if(genre != null) {
			results = genre(genre, sort, numResults, offset);
		}
		else if(title != null) {
			results = title(title, sort, numResults, offset);
		}
		else {
			return movies;
		}
		
		movies = parseResults(results);
		return movies;
	}
	
	private ResultSet search(String search, String sort, int numResults, int offset) {
		ResultSet results = null;
		String[] parts = search.split(" ");
		int len = parts.length;
		
		if(len > 1) {
			String[] temp = parts;
			parts = new String[++len];
			System.arraycopy(temp, 0, parts, 0, temp.length);
			parts[len-1] = search;
		}
		
		StringBuffer query = new StringBuffer();
        query.append("SELECT * FROM (\n");
        query.append("SELECT movies.id, title, year, director,\n");
        query.append("GROUP_CONCAT(stars.id SEPARATOR ',') AS sIds,\n");
        query.append("GROUP_CONCAT(first_name SEPARATOR ',') AS fn,\n");
        query.append("GROUP_CONCAT(last_name SEPARATOR ',') AS ln\n");
        query.append("FROM movies\n");
        query.append("JOIN stars_in_movies ON stars_in_movies.movie_id = movies.id\n");
        query.append("JOIN stars ON stars_in_movies.star_id = stars.id\n");
        query.append("GROUP BY movies.id\n");
        query.append(") AS ms JOIN (\n");
        query.append("SELECT movies.id,\n");
        query.append("GROUP_CONCAT(genres.id SEPARATOR ',') AS gIds,\n");
        query.append("GROUP_CONCAT(genres.name SEPARATOR ',') AS gn\n");
        query.append("FROM movies\n");
        query.append("JOIN genres_in_movies ON genres_in_movies.movie_id = movies.id\n");
        query.append("JOIN genres ON genres_in_movies.genre_id = genres.id\n");
        query.append("GROUP BY movies.id\n");
        query.append(") AS mg ON ms.id = mg.id\n");
        query.append("WHERE 0\n");
        query.append(buildCondition("ms.title", len));
        query.append(buildCondition("ms.year", len));
        query.append(buildCondition("ms.director", len));
        query.append(buildCondition("ms.fn", len));
        query.append(buildCondition("ms.ln", len));		
		query.append("ORDER BY " + sort + "\n");
        query.append("LIMIT " + numResults + " OFFSET " + offset);
        
        try {
        	Class.forName("com.mysql.jdbc.Driver").newInstance();	
	        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/moviedb", 
	        		"root", "1ndoviet");
	        
	        PreparedStatement statement = connection.prepareStatement(query.toString());
	        
	        for(int i=0; i < 5; i++) {
	        	for(int k=0; k < len; k++) {
	        		statement.setString(i*len + k + 1, 
	        				"%" + parts[k] + "%");
	        	}
	        }
	        
	        results = statement.executeQuery();
        }
        catch(Exception e) {
        	e.printStackTrace();
        }
        
        return results;
	}
	
	private ResultSet genre(String genre, String sort, int numResults, int offset) {
		ResultSet results = null;
		
		StringBuffer query = new StringBuffer();
        query.append("SELECT * FROM (\n");
        query.append("SELECT movies.id, title, year, director,\n");
        query.append("GROUP_CONCAT(stars.id SEPARATOR ',') AS sIds,\n");
        query.append("GROUP_CONCAT(first_name SEPARATOR ',') AS fn,\n");
        query.append("GROUP_CONCAT(last_name SEPARATOR ',') AS ln\n");
        query.append("FROM movies\n");
        query.append("JOIN stars_in_movies ON stars_in_movies.movie_id = movies.id\n");
        query.append("JOIN stars ON stars_in_movies.star_id = stars.id\n");
        query.append("GROUP BY movies.id\n");
        query.append(") AS ms JOIN (\n");
        query.append("SELECT movies.id,\n");
        query.append("GROUP_CONCAT(genres.id SEPARATOR ',') AS gIds,\n");
        query.append("GROUP_CONCAT(genres.name SEPARATOR ',') AS gn\n");
        query.append("FROM movies\n");
        query.append("JOIN genres_in_movies ON genres_in_movies.movie_id = movies.id\n");
        query.append("JOIN genres ON genres_in_movies.genre_id = genres.id\n");
        query.append("GROUP BY movies.id\n");
        query.append(") AS mg ON ms.id = mg.id JOIN(\n");
        query.append("SELECT movie_id, genre_id\n");
        query.append("FROM genres_in_movies\n");
        query.append(") AS g ON g.movie_id = ms.id\n");
        query.append("WHERE g.genre_id = ? \n");
		query.append("ORDER BY " + sort + "\n");
        query.append("LIMIT " + numResults + " OFFSET " + offset);
        
        try {
        	Class.forName("com.mysql.jdbc.Driver").newInstance();	
	        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/moviedb", 
	        		"root", "1ndoviet");
	        
	        PreparedStatement statement = connection.prepareStatement(query.toString());
	        statement.setInt(1, Integer.parseInt(genre));
	        
	        System.out.println(statement);
	        
	        results = statement.executeQuery();
        }
        catch(Exception e) {
        	e.printStackTrace();
        }
        
        return results;
	}
	
	private ResultSet title(String title, String sort, int numResults, int offset) {
		ResultSet results = null;
		
		StringBuffer query = new StringBuffer();
        query.append("SELECT * FROM (\n");
        query.append("SELECT movies.id, title, year, director,\n");
        query.append("GROUP_CONCAT(stars.id SEPARATOR ',') AS sIds,\n");
        query.append("GROUP_CONCAT(first_name SEPARATOR ',') AS fn,\n");
        query.append("GROUP_CONCAT(last_name SEPARATOR ',') AS ln\n");
        query.append("FROM movies\n");
        query.append("JOIN stars_in_movies ON stars_in_movies.movie_id = movies.id\n");
        query.append("JOIN stars ON stars_in_movies.star_id = stars.id\n");
        query.append("GROUP BY movies.id\n");
        query.append(") AS ms JOIN (\n");
        query.append("SELECT movies.id,\n");
        query.append("GROUP_CONCAT(genres.id SEPARATOR ',') AS gIds,\n");
        query.append("GROUP_CONCAT(genres.name SEPARATOR ',') AS gn\n");
        query.append("FROM movies\n");
        query.append("JOIN genres_in_movies ON genres_in_movies.movie_id = movies.id\n");
        query.append("JOIN genres ON genres_in_movies.genre_id = genres.id\n");
        query.append("GROUP BY movies.id\n");
        query.append(") AS mg ON ms.id = mg.id\n");
        query.append("WHERE ms.title LIKE ? \n");		
		query.append("ORDER BY " + sort + "\n");
        query.append("LIMIT " + numResults + " OFFSET " + offset);
        
        try {
        	Class.forName("com.mysql.jdbc.Driver").newInstance();	
	        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/moviedb", 
	        		"root", "1ndoviet");
	        
	        PreparedStatement statement = connection.prepareStatement(query.toString());
	        statement.setString(1, title + "%");
	        
	        results = statement.executeQuery();
        }
        catch(Exception e) {
        	e.printStackTrace();
        }
        
        return results;
	}
	
	private String buildCondition(String column, int len) {
		StringBuffer output = new StringBuffer();
		
		for(int i=0; i < len; i++) {
			output.append("OR " + column + " LIKE ?\n");
		}
		
		return output.toString();
	}
	
	private String mapSort(String sort) {
		if(sort == null) {
			return "title ASC";
		}
		else if(sort.equalsIgnoreCase("title-desc")) {
			return "title DESC";
		}
		else if(sort.equalsIgnoreCase("title-asc")) {
			return "title ASC";
		}
		else if(sort.equalsIgnoreCase("year-desc")) {
			return "year DESC";
		}
		else if(sort.equalsIgnoreCase("year-asc")) {
			return "year ASC";
		}
		return "title ASC";
	}
	
	private int mapNumResults(String numResults) {
		try {
			return Integer.parseInt(numResults);
		}
		catch(Exception e) {
			return 10;
		}
	}
	
	private int mapPage(String page) {
		try {
			return Integer.parseInt(page);
		}
		catch(Exception e) {
			return 1;
		}
	}
	
	private ArrayList<Movie> parseResults(ResultSet results) {
		ArrayList<Movie> movies = new ArrayList<Movie>();
		if(results == null) return movies;
		try {
			while(results.next()) {
	        	Movie movie = new Movie();
	        	movie.id = results.getInt("ms.id");
	        	movie.title = results.getString("ms.title");
	        	movie.year = results.getInt("ms.year");
	        	movie.director = results.getString("ms.director");
	        	
	        	ArrayList<Star> stars = new ArrayList<Star>();
	        	String[] starIds = results.getString("ms.sIds").split(",");
	        	String[] starFirstNames = results.getString("ms.fn").split(",");
	        	String[] starLastNames = results.getString("ms.ln").split(",");
	        	for(int i=0; i < starIds.length; i++) {
	        		Star star = new Star();
	        		star.id = Integer.parseInt(starIds[i]);
	        		star.name = starFirstNames[i] + " " + starLastNames[i];
	        		stars.add(star);
	        	}
	        	movie.stars = stars;
	        	
	        	ArrayList<Genre> genres = new ArrayList<Genre>();
	        	String[] genreIds = results.getString("mg.gIds").split(",");
	        	String[] genreNames = results.getString("mg.gn").split(",");
	        	for(int i=0; i < genreIds.length; i++) {
	        		Genre g = new Genre();
	        		g.id = Integer.parseInt(genreIds[i]);
	        		g.name = genreNames[i];
	        		genres.add(g);
	        	}
	        	movie.genres = genres;
	        	
	        	movies.add(movie);
	        }
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		return movies;
	}

}
