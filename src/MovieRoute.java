

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Models.Genre;
import Models.Movie;
import Models.Star;

/**
 * Servlet implementation class MovieRoute
 */
@WebServlet("/movies")
public class MovieRoute extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MovieRoute() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("movie");
		if(id == null) {
			System.out.println("id is null");
			request.setAttribute("movie", null);
			request.getRequestDispatcher("/views/movie.jsp").forward(request, response);
			return;
		}
		
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();	
	        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/moviedb", 
	        		"root", "1ndoviet");
	        
	        StringBuffer query = new StringBuffer();
	        query.append("SELECT * FROM (\n");
	        query.append("SELECT movies.id, title, year, director, banner_url, trailer_url,\n");
	        query.append("GROUP_CONCAT(stars.id SEPARATOR ',')AS sIds,\n");
	        query.append("GROUP_CONCAT(first_name SEPARATOR ',') AS fn,\n");
	        query.append("GROUP_CONCAT(last_name SEPARATOR ',')AS ln\n");
	        query.append("FROM movies\n");
	        query.append("JOIN stars_in_movies ON stars_in_movies.movie_id = movies.id\n");
	        query.append("JOIN stars ON stars_in_movies.star_id = stars.id\n");
	        query.append("GROUP BY movies.id\n");
	        query.append(") AS ms JOIN (\n");
	        query.append("SELECT movies.id,\n");
	        query.append("GROUP_CONCAT(genres.id SEPARATOR ',') AS gIds,\n");
	        query.append("GROUP_CONCAT(genres.name SEPARATOR ',') AS gn\n");
	        query.append("FROM movies\n");
	        query.append("JOIN genres_in_movies ON genres_in_movies.movie_id = movies.id\n");
	        query.append("JOIN genres ON genres_in_movies.genre_id = genres.id\n");
	        query.append("GROUP BY movies.id\n");
	        query.append(") AS mg ON ms.id = mg.id\n");
	        query.append("WHERE ms.id = ? \n");
	     
	        PreparedStatement statement = connection.prepareStatement(query.toString());
	        statement.setInt(1, Integer.parseInt(id));
	        
	        ResultSet results = statement.executeQuery();
	        results.next();
	        
	        Movie movie = new Movie();
        	movie.id = results.getInt("ms.id");
        	movie.title = results.getString("ms.title");
        	movie.year = results.getInt("ms.year");
        	movie.director = results.getString("ms.director");
        	movie.bannerUrl = results.getString("ms.banner_url");
        	movie.trailerUrl = results.getString("ms.trailer_url");
        	
        	ArrayList<Star> stars = new ArrayList<Star>();
        	String[] starIds = results.getString("ms.sIds").split(",");
        	String[] starFirstNames = results.getString("ms.fn").split(",");
        	String[] starLastNames = results.getString("ms.ln").split(",");
        	for(int i=0; i < starIds.length; i++) {
        		Star star = new Star();
        		star.id = Integer.parseInt(starIds[i]);
        		star.name = starFirstNames[i] + " " + starLastNames[i];
        		stars.add(star);
        	}
        	movie.stars = stars;
        	
        	ArrayList<Genre> genres = new ArrayList<Genre>();
        	String[] genreIds = results.getString("mg.gIds").split(",");
        	String[] genreNames = results.getString("mg.gn").split(",");
        	for(int i=0; i < genreIds.length; i++) {
        		Genre genre = new Genre();
        		genre.id = Integer.parseInt(genreIds[i]);
        		genre.name = genreNames[i];
        		genres.add(genre);
        	}
        	movie.genres = genres;
	        
        	request.setAttribute("movie", movie);
        	request.getRequestDispatcher("/views/movie.jsp").forward(request, response);
	        connection.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
