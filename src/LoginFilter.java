

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet Filter implementation class LoginFilter
 */
@WebFilter("/*")
public class LoginFilter implements Filter {

    /**
     * Default constructor. 
     */
    public LoginFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		System.out.println(((HttpServletRequest) request).getRequestURL().toString());
		
		if(((HttpServletRequest) request).getRequestURI().startsWith("/CS122B-02/auth") ||
				((HttpServletRequest) request).getRequestURI().startsWith("/CS122B-02/login")) {
			System.out.println("auth, no filter");
			chain.doFilter(request, response);
			return;
		}
		
		Connection connection = null;
		try {
			Cookie[] cookies = ((HttpServletRequest) request).getCookies();
			String email = null;
			String password = null;
			
			if(cookies == null) {
				((HttpServletResponse) response).sendRedirect("/CS122B-02/login");
				return;
			}
		
			for(int i=0; i < cookies.length; i++) {
				if(cookies[i].getName().equals("email")) {
					email = cookies[i].getValue();
				}
				if(cookies[i].getName().equals("password")) {
					password = cookies[i].getValue();
				}
			}
			
			Class.forName("com.mysql.jdbc.Driver").newInstance();	
	        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/moviedb", 
	        		"root", "1ndoviet");
	        
	        PreparedStatement statement = connection.prepareStatement("SELECT * FROM customers "
	        		+ "WHERE email = ? AND password = ?");
	        
	        statement.setString(1, email);
	        statement.setString(2, password);
	        ResultSet results = statement.executeQuery();
	        
	        if(results.next()) {
	        	//authorized
	        	chain.doFilter(request, response);
	        }
	        else {
	        	//not authorized
	        	((HttpServletResponse) response).sendRedirect("/CS122B-02/login?authorized=false");
	        }
	        connection.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
