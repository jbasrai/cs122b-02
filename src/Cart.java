import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Models.Genre;
import Models.Movie;
import Models.Star;

/**
 * Servlet implementation class Cart
 */
@WebServlet("/cart")
public class Cart extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Cart() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Cookie[] cookies = ((HttpServletRequest) request).getCookies();
		String cart = null;
	
		for(int i=0; i < cookies.length; i++) {
			if(cookies[i].getName().equals("cart")) {
				cart = cookies[i].getValue();
			}
		}
		
		ArrayList<String[]> cartData = new ArrayList<String[]>();
		if (cart != null && !cart.equals("")) 
		{
			String[] temp = cart.split(";");
			for (String s : temp) {
				String[] t = s.split(",");
				cartData.add(t);
			}
		}
		
		System.out.println(cart);
		System.out.println(cartData.size());
		
		String action = request.getParameter("action");
		System.out.println(action);

		if (action != null) {
			if (action.equals("add")) {
				String qty = request.getParameter("qty");
				String mov = request.getParameter("mov");
				if (qty == null || mov == null) {
					// ERROR OUT
				}
				
				System.out.println(mov);
				System.out.println(qty);
				
				boolean found = false;
				for (String[] s : cartData) {
					if (s[0].equals(mov)) {
						found = true;
						s[1] = Integer.toString(Integer.parseInt(s[1]) + 1);
					}
				}
				
				if (!found) {
					String[] item = {mov, qty};
					cartData.add(item);
				}
				
			} else if (action.equals("remove")) {
				String qty = request.getParameter("qty");
				String mov = request.getParameter("mov");
				
				for (int i = 0; i < cartData.size(); i++) {
					if (cartData.get(i)[0].equals(mov)) {
						cartData.remove(i);
						break;
					}
				}
			}
			
	        StringBuffer cartString = new StringBuffer();
			for (String[] s : cartData) {
				cartString.append(s[0] + "," + s[1] + ";");
			}
			
			response.addCookie(new Cookie("cart", cartString.toString()));
		}
		
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();	
	        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/moviedb", 
	        		"root", "1ndoviet");
	        
	        StringBuffer query = new StringBuffer();
	        query.append("SELECT * FROM (\n");
	        query.append("SELECT movies.id, title, year, director, banner_url, trailer_url,\n");
	        query.append("GROUP_CONCAT(stars.id SEPARATOR ',')AS sIds,\n");
	        query.append("GROUP_CONCAT(first_name SEPARATOR ',') AS fn,\n");
	        query.append("GROUP_CONCAT(last_name SEPARATOR ',')AS ln\n");
	        query.append("FROM movies\n");
	        query.append("JOIN stars_in_movies ON stars_in_movies.movie_id = movies.id\n");
	        query.append("JOIN stars ON stars_in_movies.star_id = stars.id\n");
	        query.append("GROUP BY movies.id\n");
	        query.append(") AS ms JOIN (\n");
	        query.append("SELECT movies.id,\n");
	        query.append("GROUP_CONCAT(genres.id SEPARATOR ',') AS gIds,\n");
	        query.append("GROUP_CONCAT(genres.name SEPARATOR ',') AS gn\n");
	        query.append("FROM movies\n");
	        query.append("JOIN genres_in_movies ON genres_in_movies.movie_id = movies.id\n");
	        query.append("JOIN genres ON genres_in_movies.genre_id = genres.id\n");
	        query.append("GROUP BY movies.id\n");
	        query.append(") AS mg ON ms.id = mg.id\n");
	        query.append("WHERE 0\n");
	        
	        for (int i = 0; i < cartData.size(); i++) {
	        	query.append("OR ms.id = " + cartData.get(i)[0] + "\n");
	        }
	     
	        PreparedStatement statement = connection.prepareStatement(query.toString());
	        ResultSet results = statement.executeQuery();
	        
	        ArrayList<Movie> movies = new ArrayList<Movie>();
	        while(results.next()) {
	        	Movie movie = new Movie();
	        	movie.id = results.getInt("ms.id");
	        	movie.title = results.getString("ms.title");
	        	movie.year = results.getInt("ms.year");
	        	movie.director = results.getString("ms.director");
	        	
	        	for (String[] s : cartData) {
					if (s[0].equals(Integer.toString(movie.id))) {
						movie.qty = Integer.parseInt(s[1]);
						System.out.println("QTY: " + movie.qty);
					}
				}
	        	
	        	ArrayList<Star> stars = new ArrayList<Star>();
	        	String[] starIds = results.getString("ms.sIds").split(",");
	        	String[] starFirstNames = results.getString("ms.fn").split(",");
	        	String[] starLastNames = results.getString("ms.ln").split(",");
	        	for(int i=0; i < starIds.length; i++) {
	        		Star star = new Star();
	        		star.id = Integer.parseInt(starIds[i]);
	        		star.name = starFirstNames[i] + " " + starLastNames[i];
	        		stars.add(star);
	        	}
	        	movie.stars = stars;
	        	
	        	ArrayList<Genre> genres = new ArrayList<Genre>();
	        	String[] genreIds = results.getString("mg.gIds").split(",");
	        	String[] genreNames = results.getString("mg.gn").split(",");
	        	for(int i=0; i < genreIds.length; i++) {
	        		Genre genre = new Genre();
	        		genre.id = Integer.parseInt(genreIds[i]);
	        		genre.name = genreNames[i];
	        		genres.add(genre);
	        	}
	        	movie.genres = genres;
	        	
	        	movies.add(movie);
	        }
	        
        	request.setAttribute("movies", movies);
        	request.getRequestDispatcher("/views/cart.jsp").forward(request, response);
	        connection.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
